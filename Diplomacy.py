#!/usr/bin/env python3

# ------------
# diplomacy_read
# ------------


def diplomacy_read(s):
    '''
    read Army, City, (Action, *City)
    '''
    #print('Diplomacy Read')
    line = s.split()
    army = str(line[0])
    city = str(line[1])
    if (len(line)==4):
        action = (line[2], line[3])
    else:
        action = (line[2],)
    return army, city, action


# ------------
# diplomacy_eval
# ------------


def diplomacy_eval(armies):
    #create solution
    for moving in armies:
        if (moving.action[0] == 'Move'):
            for target in armies:
                if (target.place == moving.action[1]):
                    target.conflict = True
                    moving.conflict = True

    for supporting in armies:
        if (supporting.action[0] == 'Support') and (supporting.conflict == False):
            for receiving in armies:
                if receiving.name == supporting.action[1]:
                    receiving.supporters += 1

    for targeted in armies:
        if ((targeted.conflict == True) and (targeted.action[0] == 'Hold' or targeted.action[0] == 'Support')):
            for moving in armies:
                if len(moving.action) == 2:
                    if targeted.place == moving.action[1]:
                        if (targeted.supporters > moving.supporters):
                            moving.alive = False
                        elif (targeted.supporters < moving.supporters):
                            targeted.alive = False
                            for another in armies:
                                if len(another.action) == 2:
                                    if targeted.place == another.action[1]:
                                        if (moving.supporters > another.supporters):
                                            another.alive = False
                                        elif (moving.supporters < another.supporters):
                                            moving.alive = False
                                        elif (moving.supporters == another.supporters) and (not moving.name == another.name):
                                            moving.alive = False
                                            another.alive = False
                        else: #if (targeted.supporters == moving.supporters) and (not moving.name == targeted.name):
                            moving.alive = False
                            targeted.alive = False

    for alive in armies:
        if not alive.alive:
            alive.place = '[dead]'
        else:
            if (len(alive.action) == 2) and (alive.action[0] == 'Move'):
                alive.place = alive.action[1]

    return armies



# -------------
# Diplomacy_print
# -------------

def diplomacy_print(w, army, place):
    '''
    print Army City
    w a writer
    '''
    w.write(str(army) + " " + str(place) + "\n")
    return

# -------------
# Diplomacy_solve
# -------------


def diplomacy_solve(r, w):
    '''
    r a reader
    w a writer
    '''
    armies = []
    for line in r:
        name, place, action = diplomacy_read(line)
        armies.append(armyclass(name, place, action))

    '''for i in armies:
        print(i.name, i.place, i.action, i.conflict, i.supporters, i.alive)'''

    diplomacy_eval(armies)

    for army in armies:
        diplomacy_print(w, army.name, army.place)

class armyclass():
    def __init__(self,name,place,action):
        self.name = name
        self.place = place
        self.action = action
        self.conflict = False
        self.supporters = 1
        self.alive = True
