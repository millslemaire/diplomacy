#!/usr/bin/env python3

# -------
# imports
# -------

import sys

from Diplomacy import diplomacy_solve

# ----
# main
# ----

if __name__ == "__main__":
    diplomacy_solve(sys.stdin, sys.stdout)

""" #pragma: no cover
% cat RunDiplomacy0.in
A Madrid Hold



% python RunDiplomacy.py < RunDiplomacyN.in > RunDiplomacyN.out
(on Windows PowerShell, the command is as follows:
"c:\> Get-Content RunDiplomacy0.in | py RunDiplomacy.py > RunDiplomacy0.out"
)


% cat RunDiplomacy0.out
A Madrid




% pydoc3 -w Diplomacy
(Note: on Windows PowerShell, the command is as follows:
"c:\> python -m pydoc -w Diplomacy"
)
# That creates the file Diplomacy.html
"""
