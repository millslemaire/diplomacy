Project #2: Diplomacy
Due: Thu, 21 Nov 2019, 10pm
100 pts, 10% of total grade.
Specification
Write a program, ideally with a partner, to solve the Diplomacy problem in Python.

Problem Background
The game Diplomacy is about warfare (ironic, ain't it?). Armies are located in various cities, and can attack cities, defend cities, and support other armies. Your job is to compute the status of various armies after they make 1 round of moves.

Input
standard in (a single test case)

Input Format: List of strings.

Every input line has three elements separated by spaces: an army, a location, and an action.

Army names are single uppercase letters, e.g. A,B,C ...

City names are single words with uppercase first letters, e.g. Paris, NewYork.

Actions have three valid values: Hold, Support, Move. The Hold action is valid by itself, and indicates the army is holding its position. The Move action must be followed by the name of a city, which indicates the target the army is moving to. The Support action must be followed by the name of an army (not a city!) which indicates which army is being supported.

Example:
A Madrid Hold

Output
standard out (a single test case)

The output is a list of armies (sorted alphabetically) and their locations. If the army is destroyed by an attack, the location is [dead]. Armies only come into conflict if they try to occupy the same city, i.e. if one army holds a city and another army tries to move to it.

If armies clash, the victor is decided by the number of supporting armies--the army with the larger number of other armies supporting it wins. If the number of supports is equal, both armies are destroyed.

If an army is attacked while supporting another army, its support becomes invalid.

Example:
A Madrid
Notes:
Initially, each army is located in a different city.
In a scenario where multiple armies attack a city, if there's exactly one army with more support that others, that army wins; otherwise all armies die.
Each army will take exactly 1 one the 3 available actions, thus there will be exactly one entry for each army in the input.
Input/Output examples
INPUT:
A Madrid Hold

OUTPUT:
A Madrid

This would indicate that a single army, army A, is holding Madrid. Since there are no other armies, the army remains alive and in the location it held.

INPUT:
A Madrid Hold
B Barcelona Move Madrid
C London Support B

OUTPUT:
A [dead]
B Madrid
C London

A is dead because B attacked while being supported by C, while A had no support. B occupies Madrid, and C stays in London because it did not move.

INPUT:
A Madrid Hold
B Barcelona Move Madrid

OUTPUT:
A [dead]
B [dead]

Since the number of supports here is equal, both armies are destroyed.

INPUT:
A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Austin Move London

OUTPUT:
A [dead]
B [dead]
C [dead]
D [dead]

Here, C attempted to support B, but was attacked by D. This invalidates C's support and turns the field into a set of two one-on-one matches where everyone dies.

INPUT:
A Madrid Hold
B Barcelona Move Madrid
C London Move Madrid

OUTPUT:
A [dead]
B [dead]
C [dead]

Here, all 3 of A, B and C are moving to Madrid. None of them have any additional support so all die.

INPUT:
A Madrid Hold
B Barcelona Move Madrid
C London Move Madrid
D Paris Support B

OUTPUT:
A [dead]
B Madrid
C [dead]
D Paris

Here, all 3 of A, B and C are moving to Madrid. B has support from D. A and C don't have any support so B wins.

INPUT:
A Madrid Hold
B Barcelona Move Madrid
C London Move Madrid
D Paris Support B
E Austin Support A

OUTPUT:
A [dead]
B [dead]
C [dead]
D Paris
E Austin

Here, all 3 of A, B and C are moving to Madrid. B has support from D and A has support from E. Since, there is no single army with largest support, all 3 of A, B and C die.

Cooperation Points
The content of this project is worth 90 points. The remaining 10 points are Cooperation points.
During the submission process, each student will independently submit an evaluation form seperately from the project submission.
In this form, you will assign 10 points to your partner based on their contribution to the project.
Your cooperation score is the cooperation points assigned to you by your partner.
You cannot give points to yourself.
You are free to discuss the point assignments with your partner and collectively plan a fair distribution. However, your final point assignments is up to you.
Your evaluations of your partner and assignment of points will be entirely anonymous, even after the project grades are released.
If you do not submit your evaluation form, you will receive 0 cooperation points.
If you receives a low cooperation score, further action may be taken.
Requirements
Estimate time to completion.
Get a GitLab account at GitLab.
Create a private code repo at GitLab, named diplomacy (https://gitlab.com/YourGitLabID/diplomacy/).
Add at least 10 issues from these requirements to the issue tracker at GitLab.
Add at least 5 more issues, one for each bug or feature, with a good description and a label.
Invite the graders to your private code repo.
Clone your private code repo onto your local directory.
Create a dev branch in git (see git branching and merging)
Only merge with the master after a successful build on GitLab CI.
Make at least 5 commits, one for each bug or feature.
If you cannot describe your changes in a sentence, you are not committing often enough.
Make meaningful commit messages identifying the corresponding issue in the issue tracker (see closing issues via commit messages).
Write unit tests in TestDiplomacy.py that test corner cases and failure cases until you have an 3 tests for the function diplomacy_solve(), confirm the expected failures, and add, commit, and push to the private code repo.
Implement and debug the simplest possible solution in diplomacy_solve() in Diplomacy.py with assertions that check pre-conditions, post-conditions, argument validity, and return-value validity, until all tests pass, and add, commit, and push to the private code repo.
Before continuing, confirm a successful build on GitLab CI.
Create 5 acceptance tests in RunDiplomacyN.in and RunDiplomacyN.out, N is 1,2,3,4,and 5, that test corner cases, and add, commit, and push to the private code repo.
Before continuing, confirm a successful build on GitLab CI.
Clone the public test repo, https://gitlab.com/fareszf/cs330e-Diplomacy-tests/, onto your local directory [make Diplomacy-tests].
It is critical that you clone the public test repo into a different directory than the one you're using for your private code repo.
Copy your unit tests and your acceptance tests to your clone of the public test repo, rename the files, do a git pull to synchronize your clone, and then add, commit and push to the public test repo.
The files MUST be named YourGitLabID-RunDiplomacy.in, YourGitLabID-RunDiplomacy.out, YourGitLabID-TestDiplomacy.py, and YourGitLabID-TestDiplomacy.out in the public test repo.
Before continuing, confirm a successful build on GitLab CI.
Run pydoc3 on Diplomacy.py, which will create Diplomacy.html [make Diplomacy.html], that then documents the interfaces to your functions.
Create inline comments if you need to explain the why of a particular implementation.
Use a consistent coding convention with good variable names, good indentation, blank lines, and blank spaces (see Google Python Style Guide).
Create a log of your commits in Diplomacy.log [make Diplomacy.log].
Obtain the git SHA with
git rev-parse HEAD

				
One submission per group by the project leader: the leader of the project must fill in the Canvas form (don't forget to rename your submission as follows "YourGitLabID-Project2.json", where the YourGitLabID is the GitLab ID of the person who is doing the submission).
Submission
The following must be provided. If anything is missing it will not be graded. You will be informed within 24 hrs of the deadline if it is incomplete, and you will have 24 hrs to resubmit it. You must e-mail the graders when you resubmit it, and you will receive a one-day late penalty. If the resubmission is still broken this process will repeat and you'll lose another one-day penalty.

Your private code repo, cs330e-Diplomacy, with graders invited as mainainers and with the following files, with these exact names, no subfolders:
.gitignore
gitlab-ci.yml
makefile
requirements.txt
Diplomacy.py
RunDiplomacy.py (do not run coverage)
RunDiplomacyN.in (5 acceptance tests each in a separate file RunDiplomacyN.in, N is 1,2,3,4)
RunDiplomacyN.out (such that each file correponds to RunDiplomacyN.in, N is 1,2,3,4, where, no coverage output)
TestDiplomacy.py (3 unit tests for the function diplomacy_solve(), run coverage)
TestDiplomacy.out (include coverage output)
Diplomacy.html (output of pydoc3)
Diplomacy.log (output of git log with at least 5 commits)
The public test repo, https://gitlab.com/fareszf/cs330e-diplomacy-tests/ with the following files, with these exact names, no subfolders:
YourGitLabID-RunDiplomacyN.in (5 acceptance tests each in a separate file RunDiplomacyN.in, N is 1,2,3,4)
YourGitLabID-RunDiplomacyN.out (5 acceptance tests each in a separate file RunDiplomacyN.out, N is 1,2,3,4, no coverage output)
YourGitLabID-TestDiplomacy.py (3 unit tests for the function diplomacy_solve(), run coverage)
YourGitLabID-TestDiplomacy.out (include coverage output)
GitLab issue tracker with at least 15 issues.
It is your responsibility to protect your code from the rest of the students in the class. If your code gets out, you are as guilty as the recipient of academic dishonesty.
Canvas form with GitLab Pipelines and time estimate.
Rubrics
[10 pts] Quality of the repo.
at least 5 commits
git log
[20 pts] Quality of the code.
[10 pts] Quality of the issues.
at least 15 issues
[20 pts] Quality of the unit tests.
3 unit tests for the function diplomacy_solve()
coverage results
[10 pts] Quality of the acceptance tests.
5 acceptance tests
[10 pts] Quality of the integration.
GitLab CI log
[10 pts] Quality of the documentation.
pydoc output
[10 pts] Cooperation points.
Bonuses
Five acceptance tests will be used to test your code. You lose a max of 10 pts, 2 pts for every test your code fails to pass.
You loose 2 points for each missing requirement.
You can earn another 10 bonus pts, if you work with a partner using pair programming and vouch for the fact that you worked on the project together for more than 75% of the time.
Only one solution must be turned in for the pair. If two solutions are turned in, there will be a 10% penalty, and the later one will be graded.
Bonus pts will not increase the total score beyond the max score.
